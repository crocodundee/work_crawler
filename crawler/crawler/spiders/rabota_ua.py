# -*- coding: utf-8 -*-
from scrapy import Spider
from ..items import CrawlerItem
from selenium.webdriver import Chrome, ChromeOptions


class RabotaUaSpider(Spider):
    name = 'rabota_ua'
    allowed_domains = ['www.rabota.ua']
    start_urls = ['https://www.rabota.ua']

    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.RabotaPipeline': 400
        }
    }

    def __init__(self, position=None, city=None):
        self.items = CrawlerItem()
        self.position = position
        self.city = city

    def parse(self, response):
        options = ChromeOptions()
        options.add_argument("--headless")
        driver = Chrome(options=options)
        driver.get(self.start_urls[0])
        job_input = driver.find_element_by_id('ctl00_content_vacSearch_Keyword')
        job_input.send_keys(self.position)
        city_input = driver.find_element_by_id('ctl00_content_vacSearch_CityPickerWork_inpCity')
        city_input.clear()
        city_input.send_keys(self.city)
        submit = driver.find_element_by_id('ctl00_content_vacSearch_lnkSearch')
        submit.click()
        result_url = driver.current_url
        driver.quit()

        yield response.follow(result_url, callback=self.start_parse, dont_filter=True)

    def start_parse(self, response):
        jobs = response.css('tr')

        for job in jobs:
            position = job.css('h3.f-vacancylist-vacancytitle').css('a::text').get()
            company = job.css('p.f-vacancylist-companyname').css('a::text').get()
            pos_link = job.css('h3.f-vacancylist-vacancytitle').css('a::attr(href)').get()

            if pos_link:
                yield response.follow(pos_link,
                                      dont_filter=True,
                                      callback=self.get_detail,
                                      meta={'position': position, 'company': company})

        next_page = response.css('a#ctl00_content_VacancyList_gridList_ctl23_linkNext::attr(href)').get()
        if next_page:
            yield response.follow(next_page, callback=self.start_parse, dont_filter=True)

    def get_detail(self, response):
        description = response.css('div.f-vacancy-description-inner-content, div.d_des').extract()

        self.items['position'] = response.meta.get('position').strip()
        self.items['company'] = response.meta.get('company').strip()
        self.items['description'] = description

        yield self.items



