from scrapy import Spider
from scrapy.http import FormRequest
from ..items import CrawlerItem
from selenium.webdriver import Chrome, ChromeOptions


class DouUaSpider(Spider):
    name = 'dou_ua'
    allowed_domains = ['jobs.dou.ua']
    start_urls = ['https://jobs.dou.ua']
    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.DouPipeline': 400
        }
    }

    def __init__(self, position=None, city=None):
        self.items = CrawlerItem()
        self.position = position
        self.city = city

    def parse(self, response):
        options = ChromeOptions()
        options.add_argument("--headless")
        driver = Chrome(options=options)
        driver.get(self.start_urls[0])
        job_input = driver.find_element_by_name('search')
        job_input.clear()
        job_input.send_keys(self.position)
        submit = driver.find_element_by_class_name('btn-search')
        submit.click()
        city_url = driver.current_url

        yield response.follow(city_url,
                              meta={'city': self.city},
                              callback=self.start_parse)

    def start_parse(self, response):
        links = response.css('div.b-region-filter ul')[-1].css('li')
        url = ''
        for li in links:
            city = li.css('a::text').get()
            if city == response.meta.get('city'):
                url = li.css('a::attr(href)').get()
                break

        yield response.follow(url, callback=self.get_detail, dont_filter=True)

    def get_detail(self, response):
        urls = response.css('a.vt::attr(href)').extract()

        for url in urls:
            yield response.follow(url, callback=self.get_description, dont_filter=True)

    def get_description(self, response):
        position = response.css('h1.g-h2::text').get()
        company = response.css('div.l-n a::text').get()
        description = response.css('div.l-vacancy').extract()

        self.items['position'] = position
        self.items['company'] = company
        self.items['description'] = description

        yield self.items










