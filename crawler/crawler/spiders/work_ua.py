# -*- coding: utf-8 -*-
from ..items import CrawlerItem
from scrapy import Spider, FormRequest


class WorkUaSpider(Spider):
    name = 'work_ua'
    allowed_domains = ['www.work.ua']
    start_urls = ['https://www.work.ua']
    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.WorkPipeline': 400
        }
    }

    def __init__(self, position=None, city=None):
        self.position = position
        self.city = city

    def parse(self, response):
        return FormRequest.from_response(response,
                                         formdata={
                                             'search': self.position,
                                             'Місто': self.city
                                         },
                                         callback=self.start_parse)

    def start_parse(self, response):
        junior = response.css('div.job-link')

        for jun in junior:
            link = jun.css('a::attr(href)').extract_first()
            yield response.follow(link, callback=self.get_detail)

        pagination = response.css("ul.pagination.hidden-xs li a::attr(href)").extract()

        if pagination:
            yield response.follow(pagination[-1], callback=self.start_parse)

    def get_detail(self, response):
        items = CrawlerItem()

        name = response.css('h1#h1-name::text, h1#h1-name span.highlight-result::text').extract()
        description = response.css('div#job-description').extract()
        company = response.css('.dl-horizontal a::attr(title)').get()

        items['position'] = "".join(name)
        items['company'] = company
        items['description'] = description

        yield items
