from scrapy import spiderloader
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

settings = get_project_settings()


def get_spiders_cls():
    spider_loader = spiderloader.SpiderLoader.from_settings(settings)
    spiders_cls = [spider_loader.load(name) for name in spider_loader.list()]
    return spiders_cls


def get_search_data():
    print("Position: ")
    position = input()
    print("City: ")
    city = input()
    return position, city


if __name__ == '__main__':
    position, city = get_search_data()
    spiders_cls = get_spiders_cls()
    process = CrawlerProcess(settings)
    print("RESULT FOR {} in {}:".format(position.upper(), city.upper()))
    for spider in spiders_cls:
        process.crawl(spider, position=position, city=city)
    process.start()



