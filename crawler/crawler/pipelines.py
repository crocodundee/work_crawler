import sqlite3 as db
import os

PROJECT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class BasicPipeline(object):
    def __init__(self, db_name, tb_name):
        self.db = f'{PROJECT}\crawler\{db_name}.sqlite3'
        self.db_name = db_name
        self.tb_name = tb_name
        self.create_connection()
        self.create_table()

    def create_connection(self):
        self.conn = db.connect(self.db)
        self.curr = self.conn.cursor()

    def create_table(self):
        db_exist = f"""DROP TABLE IF EXISTS {self.db_name}"""
        tb_exist = f"""DROP TABLE {self.tb_name}"""
        create_table = f"""CREATE TABLE {self.tb_name}(position text, company text, description text)"""
        self.curr.execute(db_exist)
        self.curr.execute(tb_exist)
        self.curr.execute(create_table)

    def store_db(self, item):
        insert_command = f"""INSERT INTO {self.tb_name} VALUES (?,?,?)"""
        self.curr.execute(insert_command, (item['position'], item['company'], item['description'][0]))
        self.conn.commit()


class WorkPipeline(BasicPipeline):
    def __init__(self):
        BasicPipeline.__init__(self, 'worker', 'work_ua')

    def process_item(self, item, spider):
        self.store_db(item)


class RabotaPipeline(BasicPipeline):
    def __init__(self):
        BasicPipeline.__init__(self, 'worker', 'rabota_ua')

    def process_item(self, item, spider):
        self.store_db(item)


class DouPipeline(BasicPipeline):
    def __init__(self):
        BasicPipeline.__init__(self, 'worker', 'dou_ua')

    def process_item(self, item, spider):
        self.store_db(item)
