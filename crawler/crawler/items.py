import scrapy


class CrawlerItem(scrapy.Item):
    position = scrapy.Field()
    company = scrapy.Field()
    description = scrapy.Field()

